
It is a keras based implementation of deep siamese Bidirectional LSTM network to capture pritien funtional similarity using 3-mer embeddings.

#### Install dependencies

`pip install -r requirements.txt`

### Usage

```python
from operator import itemgetter
from keras.models import load_model
model = load_model("../siamese_v1_model_threeN_v1",compile=False')
model.compile(loss='binary_crossentropy', metrics=['acc',auroc], optimizer='adam')

test_sentence_pairs = [(protien1),(protien2)]
test_data_x1, test_data_x2, leaks_test = create_test_data(tokenizer,test_sentence_pairs,  siamese_config['MAX_SEQUENCE_LENGTH'])

preds = list(model.predict([test_data_x1, test_data_x2, leaks_test], verbose=1).ravel())
results = [(x, y, z) for (x, y), z in zip(test_sentence_pairs, preds)]
results.sort(key=itemgetter(2), reverse=True)
print results
```

### Model References:

1. [Siamese Recurrent Architectures for Learning Sentence Similarity (2016)](https://www.aaai.org/ocs/index.php/AAAI/AAAI16/paper/view/12195)
